package Event;

import java.io.Serializable;

/**
 * This is the interface for event
 *
 * @author Team38
 *
 */
public interface IEvent  {
    public String getEventInfo();

    public String getEventData();
}
